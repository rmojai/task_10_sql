from flask_restful import Api

from api.api_resource import api_school, Groups, Students, Courses, GroupId, StudentId, CourseId

api = Api(api_school)

api.add_resource(Groups, '/groups/')
api.add_resource(Students, '/students/')
api.add_resource(Courses, '/courses/')

api.add_resource(GroupId, '/groups/<groupId>')
api.add_resource(StudentId, '/students/<studentId>')
api.add_resource(CourseId, '/courses/<courseId>')
