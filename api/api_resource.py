from flask import Blueprint, jsonify, request
from flask_restful import Resource
from sqlalchemy import func

from app.models import db, Student, Group, Course

api_school = Blueprint('api_school', __name__, url_prefix='/api/v1')


class Groups(Resource):
    @staticmethod
    def get():  # Get a list of groups.
        num = request.args.get('number')
        if num:  # Find all groups with less or equals student count.
            report = db.session.query(func.count(Student.group_id), Group.group_name, Group.id). \
                join(Group, Student.group_id == Group.id). \
                group_by(Group.group_name, Group.id).having(func.count(Student.group_id) <= num).all()
            group_list = []
            for row in report:
                group_list.append({'id': row.id, 'group_name': row.group_name})
            return jsonify({f'List all groups with less or equals student count {num}': group_list})
        else:
            report = Group.query.all()
            group_list = []
            for row in report:
                group_list.append({'id': row.id, 'group_name': row.group_name})
            return jsonify(group_list)

    @staticmethod
    def post():  # Add new group
        group_name = request.args.get('group_name')

        db.session.add(Group(group_name=group_name))
        db.session.commit()

        group = Group.query.order_by(Group.id.desc()).first()
        new_group = f'id: {group.id}, group_name: {group.group_name}'

        return jsonify({f'Info of group': new_group})


class Students(Resource):
    @staticmethod
    def get():  # Get a list of students.
        report = Student.query.all()
        students_list = []
        for row in report:
            students_list.append({
                'id': row.id, 'group_id': row.group_id, 'first_name': row.first_name, 'last_name': row.last_name})
        return jsonify(students_list)

    @staticmethod
    def post():  # Add new student
        first_name = request.args.get('first_name')
        last_name = request.args.get('last_name')
        group_id = request.args.get('group_id')

        course_name = request.args.get('course_name')
        list_course_name = course_name.split(',')

        db.session.add(Student(first_name=first_name, last_name=last_name, group_id=group_id))
        db.session.commit()

        for i in list_course_name:
            student = Student.query.order_by(Student.id.desc()).first()
            course = db.session.query(Course).filter(Course.course_name == i).first()
            student.courses.append(course)
            db.session.commit()

        student = Student.query.order_by(Student.id.desc()).first()
        new_student = f'id: {student.id}, first_name: {student.first_name}, last_name: {student.last_name}, ' \
                      f'group_id: {student.group_id}, courseId: {student.courses}'
        return jsonify({f'New student': new_student})


class Courses(Resource):
    @staticmethod
    def get():  # Get a list of courses.
        course_name = request.args.get('course_name')
        report = Course.query.all()
        if course_name:  # Find all students related to the course with a given name.
            query_course = Student.query.join(Student.courses).filter(Course.course_name == course_name).all()
            course_list = []
            for row in query_course:
                course_list.append(f'id {row.id}, first_name: {row.first_name}, last_name: {row.last_name}')
            return jsonify({f'List all students related to the {course_name} course': course_list})
        else:
            course_list = []
            for row in report:
                student = []
                for i in row.students:
                    student.append(i.id)
                course_list.append({'id': row.id, 'course_name': row.course_name, 'students': student})
            return jsonify(course_list)

    @staticmethod
    def post():  # Add new course
        course_name = request.args.get('course_name')
        description = request.args.get('description')

        students = request.args.get('studentId')
        list_students = list(map(int, students.split(',')))

        db.session.add(Course(course_name=course_name, description=description))
        db.session.commit()

        no_student = []

        for i in list_students:
            course = Course.query.order_by(Course.id.desc()).first()
            student = db.session.query(Student).filter(Student.id == i).first()
            if student is None:
                no_student.append(i)
            else:
                student.courses.append(course)
                db.session.commit()

        course = Course.query.order_by(Course.id.desc()).first()
        new_course = f'id: {course.id}, course_name: {course.course_name}, description: {course.description}, ' \
                     f'students: {course.students}'
        if no_student:
            return jsonify(
                {f'Added new course, but not founded student {no_student} to assign in a new course': new_course})
        else:
            return jsonify({f'Added new course ': new_course})


class GroupId(Resource):
    @staticmethod
    def get(groupId):  # Get info of group by groupId
        report = Group.query.filter(Group.id == groupId).first()
        if report is None:
            return jsonify({f'Info of group': "Group not found"})
        else:
            group_info = f'id: {report.id}, group_name: {report.group_name}'
            return jsonify({f'Info of group': group_info})

    @staticmethod
    def delete(groupId):  # Delete group by groupId
        report = Group.query.filter(Group.id == groupId).first()
        if report is None:
            return jsonify({f'Group with ID: {groupId}': 'Not found'})
        else:
            db.session.query(Group).filter(Group.id == groupId).delete(synchronize_session=False)
            db.session.commit()

            report = Group.query.filter(Group.id == groupId).first()
            if report is None:
                return jsonify({f'Deleted group with ID: {groupId}': 'successful'})

    @staticmethod
    def put(groupId):  # Modify info of group by groupId
        group_name = request.args.get('group_name')
        report = Group.query.filter(Group.id == groupId).first()
        if report is None:
            return jsonify({f'Info of group': "Group not found"})
        else:
            report.group_name = group_name
            db.session.commit()

            report = Group.query.filter(Group.id == groupId).first()
            modify_group = f'id: {report.id}, group_name: {report.group_name}'
            return jsonify({f'Modified info about group': modify_group})


class StudentId(Resource):
    @staticmethod
    def get(studentId):  # Get info of student by studentId
        report = Student.query.filter(Student.id == studentId).first()
        if report is None:
            return jsonify({f'Info of student': "Student not found"})
        else:
            student_info = f'id: {report.id}, group_id: {report.group_id}, first_name: {report.first_name},' \
                           f' last_name: {report.last_name}, courses: {report.courses}'
            return jsonify({f'Info of student': student_info})

    @staticmethod
    def delete(studentId):  # Delete student by studentId
        report = Student.query.filter(Student.id == studentId).first()
        if report is None:
            return jsonify({f'Student with ID: {studentId}': 'Not found'})
        else:
            db.session.query(Student).filter(Student.id == studentId).delete(synchronize_session=False)
            db.session.commit()

            report = Student.query.filter(Student.id == studentId).first()
            if report is None:
                return jsonify({f'Deleted student with ID: {studentId}': 'successful'})

    @staticmethod
    def put(studentId):  # Modify  info of student by studentId
        first_name = request.args.get('first_name')
        last_name = request.args.get('last_name')
        group_id = request.args.get('group_id')
        course_name_assign = request.args.get('course_name_assign')
        course_name_remove = request.args.get('course_name_remove')

        report = Student.query.filter(Student.id == studentId).first()
        if report is None:
            return jsonify({f'Info of student': "Student not found"})
        if first_name:
            report.first_name = first_name
        if last_name:
            report.last_name = last_name
        if group_id:
            report.group_id = group_id
        if course_name_assign:  # Add a student to the course (from a list)
            list_course_name = course_name_assign.split(',')
            for i in list_course_name:
                student = Student.query.filter(Student.id == studentId).first()
                course = db.session.query(Course).filter(Course.course_name == i).first()
                student.courses.append(course)
                db.session.commit()
        no_course = []
        if course_name_remove:  # Remove the student from one of his or her courses
            list_course_name = course_name_remove.split(',')
            for i in list_course_name:
                student = Student.query.filter(Student.id == studentId).first()
                course = db.session.query(Course).filter(Course.course_name == i).first()
                if course in student.courses:
                    student.courses.remove(course)
                    db.session.commit()
                else:
                    no_course.append(i)
        db.session.commit()
        student = Student.query.filter(Student.id == studentId).first()
        modify_student = f'id: {student.id}, first_name: {student.first_name}, last_name: {student.last_name}, ' \
                         f'group_id: {student.group_id}, courseId: {student.courses}'
        if no_course:
            return jsonify(
                {f'Student info had modified, but not founded courses {no_course} to remove': modify_student})
        else:
            return jsonify({f'Modified info about the student': modify_student})


class CourseId(Resource):
    @staticmethod
    def get(courseId):  # Get info of course by courseId
        report = Course.query.filter(Course.id == courseId).first()
        if report is None:
            return jsonify({f'Info of course': "Course not found"})
        else:
            course_info = f'id: {report.id}, course_name: {report.course_name}, students: {report.students}'
            return jsonify({f'Info of course': course_info})

    @staticmethod
    def delete(courseId):  # Delete course by courseId
        report = Course.query.filter(Course.id == courseId).first()
        if report is None:
            return jsonify({f'Course with ID: {courseId}': 'Not found'})
        else:
            db.session.query(Course).filter(Course.id == courseId).delete(synchronize_session=False)
            db.session.commit()

            report = Course.query.filter(Course.id == courseId).first()
            if report is None:
                return jsonify({f'Deleted course with ID: {courseId}': 'successful'})

    @staticmethod
    def put(courseId):  # Modify info of course by courseId
        course_name = request.args.get('course_name')
        description = request.args.get('description')
        students_assign = request.args.get('studentId_assign')
        students_remove = request.args.get('studentId_remove')

        report = Course.query.filter(Course.id == courseId).first()
        if report is None:
            return jsonify({f'Info of course': "Course not found"})
        if course_name:
            report.course_name = course_name
        if description:
            report.description = description
        no_student_assign = []
        if students_assign:  # Add a student to the course (from a list)
            list_students_assign = list(map(int, students_assign.split(',')))
            for i in list_students_assign:
                course = Course.query.filter(Course.id == courseId).first()
                student = db.session.query(Student).filter(Student.id == i).first()
                if student is None:
                    no_student_assign.append(i)
                else:
                    student.courses.append(course)
                    db.session.commit()
        no_student_remove = []
        if students_remove:  # Remove the student from one of his or her courses
            list_students_remove = list(map(int, students_remove.split(',')))
            for i in list_students_remove:
                course = Course.query.filter(Course.id == courseId).first()
                student = db.session.query(Student).filter(Student.id == i).first()
                if student is None:
                    no_student_remove.append(i)
                elif course in student.courses:
                    student.courses.remove(course)
                    db.session.commit()
                else:
                    no_student_remove.append(i)

        db.session.commit()
        course = Course.query.filter(Course.id == courseId).first()
        modify_course = f'id: {course.id}, course_name: {course.course_name}, description: {course.description}, ' \
                        f'students: {course.students}'
        if no_student_assign and no_student_remove:
            return jsonify(
                {
                    f'Course had modified, but not founded student {no_student_assign} to assign in a course and'
                    f' not founded student {no_student_remove} to remove in a course ': modify_course})
        if no_student_assign:
            return jsonify(
                {
                    f'Course had modified, but not founded student {no_student_assign} to assign in a course': modify_course})
        if no_student_remove:
            return jsonify(
                {
                    f'Course had modified, but not founded student {no_student_remove} to remove in a course': modify_course})
        else:
            return jsonify({f'Modified info about course ': modify_course})
