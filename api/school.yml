openapi: 2.0
info:
  title: REST API SCHOOL
  description: REST API SCHOOL
  version: 0.1
paths:
  /api/v1/groups/:
    get:
      tags:
        - groups
      summary: Get a list of groups
      description: Get a list of groups
      parameters:
        - name: number
          in: query
          required: false
          description: enter max number of student in group
          type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    post:
      tags:
        - groups
      summary: Add new group
      description: Add new group
      parameters:
        - name: group_name
          in: query
          required: true
          description: enter name of group. The name should contain 2 capital characters, hyphen, 2 numbers. Example "FR-34"
          type: string
          pattern: '[A-Z]{2}-\d{2}'
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
  /api/v1/students/:
    get:
      tags:
        - students
      summary: Get a list of students
      description: Get a list of students
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    post:
      tags:
        - students
      summary: Add new student
      description: Add new student
      parameters:
        - name: first_name
          in: query
          required: true
          description: enter first name of student
          type: string
        - name: last_name
          in: query
          required: true
          description: enter last name of student
          type: string
        - name: group_id
          in: query
          required: true
          description: select group number
          type: integer
          enum: [ 1,2,3,4,5,6,7,8,9,10 ]
        - name: course_name
          in: query
          required: false
          description: select one or more courses to assign the student
          type: array
          items:
            type: string
            enum: [ Transfiguration,
                    Charms,
                    Potions,
                    Astronomy,
                    History of Magic,
                    Herbology,
                    Arithmancy,
                    Muggle Studies,
                    Divination,
                    Flying lessons ]
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
  /api/v1/courses/:
    get:
      tags:
        - courses
      summary: Get a list of courses
      description: Get a list of courses
      parameters:
        - name: course_name
          in: query
          required: false
          description: select a course name to get a list of students assigned to the course
          type: string
          enum: [ Transfiguration,
                  Charms,
                  Potions,
                  Astronomy,
                  History of Magic,
                  Herbology,
                  Arithmancy,
                  Muggle Studies,
                  Divination,
                  Flying lessons ]
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    post:
      tags:
        - courses
      summary: Add new course
      description: Add new course
      parameters:
        - name: course_name
          in: query
          required: true
          description: enter name of course.
          type: string
        - name: description
          in: query
          required: true
          description: enter description of course.
          type: string
        - name: studentId
          in: query
          required: false
          description: enter separated studentId assigned of course.
          type: array
          items:
            type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
  /api/v1/groups/{groupId}:
    get:
      tags:
        - group
      summary: Get info of group by groupId
      description: Get info of group by groupId
      parameters:
        - name: groupId
          in: path
          required: true
          description: enter groupId
          type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    delete:
      tags:
        - group
      summary: Delete group by groupId
      description: Delete group by groupId
      parameters:
        - name: groupId
          in: path
          required: true
          description: enter groupId
          type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    put:
      tags:
        - group
      summary: Modify group by groupId
      description: Modify group by groupId
      parameters:
        - name: groupId
          in: path
          required: true
          description: enter groupId
          type: integer
        - name: group_name
          in: query
          required: false
          description: enter new name of group. The name should contain 2 capital characters, hyphen, 2 numbers. Example "FR-34"
          type: string
          pattern: '[A-Z]{2}-\d{2}'
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
  /api/v1/students/{studentId}:
    get:
      tags:
        - student
      summary: Get info of student by studentId
      description: Get info of student by studentId
      parameters:
        - name: studentId
          in: path
          required: true
          description: enter studentId
          type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    delete:
      tags:
        - student
      summary: Delete student by studentId
      description: Delete student by studentId
      parameters:
        - name: studentId
          in: path
          required: true
          description: enter studentId
          type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    put:
      tags:
        - student
      summary: Modify student by studentId
      description: Modify student by studentId
      parameters:
        - name: studentId
          in: path
          required: true
          description: enter studentId
          type: integer
        - name: first_name
          in: query
          required: false
          description: enter first name of student
          type: string
        - name: last_name
          in: query
          required: false
          description: enter last name of student
          type: string
        - name: group_id
          in: query
          required: false
          description: select group number
          type: integer
          enum: [ 1,2,3,4,5,6,7,8,9,10 ]
        - name: course_name_assign
          in: query
          required: false
          description: select one or more courses to assign the student
          type: array
          items:
            type: string
            enum: [ Transfiguration,
                    Charms,
                    Potions,
                    Astronomy,
                    History of Magic,
                    Herbology,
                    Arithmancy,
                    Muggle Studies,
                    Divination,
                    Flying lessons ]
        - name: course_name_remove
          in: query
          required: false
          description: select one or more courses to remove the student
          type: array
          items:
            type: string
            enum: [ Transfiguration,
                    Charms,
                    Potions,
                    Astronomy,
                    History of Magic,
                    Herbology,
                    Arithmancy,
                    Muggle Studies,
                    Divination,
                    Flying lessons ]
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
  /api/v1/courses/{courseId}:
    get:
      tags:
        - course
      summary: Get info of course by courseId
      description: Get info of course by courseId
      parameters:
        - name: courseId
          in: path
          required: true
          description: enter courseId
          type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    delete:
      tags:
        - course
      summary: Delete course by courseId
      description: Delete course by courseId
      parameters:
        - name: courseId
          in: path
          required: true
          description: enter courseId
          type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
    put:
      tags:
        - course
      summary: Modify course by courseId
      description: Modify course by courseId
      parameters:
        - name: courseId
          in: path
          required: true
          description: enter courseId
          type: integer
        - name: course_name
          in: query
          required: false
          description: enter name of course.
          type: string
        - name: description
          in: query
          required: false
          description: enter description of course.
          type: string
        - name: studentId_assign
          in: query
          required: false
          description: enter separated studentId to assign in course.
          type: array
          items:
            type: integer
        - name: studentId_remove
          in: query
          required: false
          description: enter separated studentId to remove from course.
          type: array
          items:
            type: integer
      responses:
        200:
          description: Successful response
        404: # status code
          description: Not found response
