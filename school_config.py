class Config(object):
    SECRET_KEY = 'dev'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://roman:ivanka@localhost:5432/school'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    NUMBER_OF_GROUPS = 10
    NUMBER_OF_STUDENTS = 200
    NUMBER_OF_STUDENTS_IN_GROUP = 22
    NUMBER_OF_COURSES_STUDENT_HAS = 3


class Testing(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
