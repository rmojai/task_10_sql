import pytest
from flask import json

import school_config
from app import create_app
from app.create_data import create_tables, fill_groups, fill_students, fill_courses, assign_students, assign_relation

baseUrl = "http://127.0.0.1:5000/api/v1"


@pytest.fixture()
def school_test():
    school_test = create_app(school_config.Testing)
    school_test.app_context().push()
    create_tables()
    fill_groups()
    fill_students()
    fill_courses()
    assign_students()
    assign_relation()
    yield school_test


@pytest.fixture()
def client(school_test):
    client = school_test.test_client()
    return client


def test_groups_get(client):
    path = "/groups/?number=2"
    response = client.get(baseUrl + path)
    assert response.status_code == 200
    assert b'"List all groups with less or equals student count 2":' \
           in response.data


def test_groups_post(client):
    path = "/groups/?group_name=GG-22"
    response = client.post(baseUrl + path)
    assert response.status_code == 200
    assert b'"Info of group": "id: 11, group_name: GG-22"' in response.data


def test_group_delete(client):
    path = "/groups/2"
    response = client.delete(baseUrl + path)
    assert response.status_code == 200
    assert b'"Deleted group with ID: 2": "successful"' in response.data


def test_group_put(client):
    path = "/groups/3?group_name=GG-44"
    response = client.put(baseUrl + path)
    assert response.status_code == 200
    assert b'"Modified info about group": "id: 3, group_name: GG-44"' in response.data


def test_group_get(client):
    path = "/groups/2"
    response = client.get(baseUrl + path)
    assert response.status_code == 200
    assert b'"Info of group": "id: 2, group_name:' \
           in response.data


def test_students_get(client):
    path = "/students/"
    response = client.get(baseUrl + path)
    assert response.status_code == 200
    assert b'"List all students":' \
           in response.data


def test_students_post(client):
    path = "/students/?first_name=Roman&last_name=Mozhaiev&group_id=1&course_name=Transfiguration"
    response = client.post(baseUrl + path)
    assert response.status_code == 200
    assert b'"New student": "id: 201, first_name: Roman, last_name: Mozhaiev, group_id: 1, courseId: [<Course 1>]"' \
           in response.data


def test_student_delete(client):
    path = "/students/2"
    response = client.delete(baseUrl + path)
    assert response.status_code == 200
    assert b'"Deleted student with ID: 2": "successful"' in response.data


def test_student_put(client):
    path = "/students/4?first_name=Roman&last_name=Mozhaiev&group_id=5&course_name_assign=" \
           "Transfiguration,Charms&course_name_remove=Potions,Astronomy"
    response = client.put(baseUrl + path)
    assert response.status_code == 200
    assert b'to remove": "id: 4, first_name: Roman, last_name: Mozhaiev, group_id: 5, courseId:' in response.data


def test_student_get(client):
    path = "/students/2"
    response = client.get(baseUrl + path)
    assert response.status_code == 200
    assert b'"Info of student": "id: 2, group_id:' \
           in response.data


def test_courses_get(client):
    path = "/courses/?course_name=Transfiguration"
    response = client.get(baseUrl + path)
    assert response.status_code == 200
    assert b'"List all students related to the Transfiguration course":' \
           in response.data


def test_courses_post(client):
    path = "/courses/?course_name=Biologic&description=Biologic&studentId=22,33"
    response = client.post(baseUrl + path)
    assert response.status_code == 200
    assert b'"Added new course ": "id: 11, course_name: Biologic, description: Biologic, students: [<Student 22>, <Student 33>]"' in response.data


def test_course_delete(client):
    path = "/courses/2"
    response = client.delete(baseUrl + path)
    assert response.status_code == 200
    assert b'"Deleted course with ID: 2": "successful"' in response.data


def test_course_put(client):
    path = "/courses/10?course_name=Mathematics&description=Mathematics&studentId_assign=22,222&studentId_remove=33,333"
    response = client.put(baseUrl + path)
    assert response.status_code == 200
    assert b'"Course had modified, but not founded student [222] ' \
           b'to assign in a course and not founded student [33, 333] ' \
           b'to remove in a course ": "id: 10, course_name: Mathematics, description: Mathematics, students:' \
           in response.data


def test_course_get(client):
    path = "/courses/2"
    response = client.get(baseUrl + path)
    assert response.status_code == 200
    assert b'"Info of course": "id: 2, course_name:' \
           in response.data


def test_groups_json(client):
    path = "/groups/?number=2"
    response = client.get(baseUrl + path)
    assert response.is_json is True


def test_groups_json2(client):
    path = "/groups/?number=2"
    response = client.get(baseUrl + path)
    data = json.loads(response.get_data(as_text=True))
    assert data.keys() == {"List all groups with less or equals student count 2"}


if __name__ == '__main__':
    pytest.main()
