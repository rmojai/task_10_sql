import random
import string

import click
from flask.cli import with_appcontext

import school_config
from app.models import db, Group, Course, Student

courses = {'Transfiguration': 'Transfiguration is essentially the art of changing the properties of an object.',
           'Charms': 'Charms is the class that teaches how to develop incantations for the uses of bewitchment.',
           'Potions': 'Potions is described as the art of creating mixtures with magical effects.',
           'Astronomy': 'Lessons involve observations of the night skies with telescopes.',
           'History of Magic': 'History of Magic is the study of magical history.',
           'Herbology': 'Herbology is the study of magical plants and how to take care of, utilise and combat them.',
           'Arithmancy': 'Arithmancy is a branch of magic concerned with the magical properties of numbers.',
           'Muggle Studies': 'Muggle Studies is a class that involves the study of the Muggle.',
           'Divination': 'Divination is the art of predicting the future.',
           'Flying lessons': 'Flying is the class that teaches the use of broomsticks made for the use of flying.',
           }
first_names = ['Aragorn', 'Isildur', 'Bilbo', 'Boromir', 'Legolas', 'Sam', 'Faramir', 'Frodo', 'Gandalf', 'Gimli',
               'Saruman', 'Pippin', 'Merry', 'Haldir', 'Gollum', 'Galadriel', 'Elrond', 'Denethor', 'Arwen', 'Aratorn']
last_names = ['Mortensen', 'Tyler', 'Holm', 'Bean', 'Noble', 'Urban', 'Otto', 'Wood', 'Blanchett', 'McKellen',
              'Parker', 'Norell', 'Bloom', 'Monaghan', 'Boyd', 'Astin', 'Lee', 'Hill', 'Dourif', 'Makoare']


@click.command(name='create')
@with_appcontext
def create():
    create_tables()
    fill_groups()
    fill_students()
    fill_courses()
    assign_students()
    assign_relation()


def create_tables():
    db.create_all()


def fill_groups():
    num = school_config.Config.NUMBER_OF_GROUPS
    for i in range(num):
        chars = string.ascii_uppercase
        digits = string.digits
        random_chars = ''.join(random.sample(chars, 2))
        random_digits = ''.join(random.sample(digits, 2))

        data = Group(group_name=random_chars + '-' + random_digits)
        db.session.add(data)
        db.session.commit()


def fill_students():
    num = school_config.Config.NUMBER_OF_STUDENTS
    for i in range(num):
        random.shuffle(first_names)
        random.shuffle(last_names)

        data = Student(first_name=first_names[0], last_name=last_names[0])
        db.session.add(data)
        db.session.commit()


def fill_courses():
    for course, description in courses.items():
        data = Course(course_name=course, description=description)
        db.session.add(data)
        db.session.commit()


def assign_students():
    num = school_config.Config.NUMBER_OF_STUDENTS_IN_GROUP

    query_group = db.session.query(Group).all()
    for group in query_group:
        query_student = db.session.query(Student).filter(Student.group_id.is_(None))
        query_student = list(query_student)
        random.shuffle(query_student)
        for student in query_student:
            count_g = db.session.query(Student).filter(Student.group_id == group.id).count()
            if count_g < num:
                student.group_id = group.id
                db.session.commit()


def assign_relation():
    num = school_config.Config.NUMBER_OF_COURSES_STUDENT_HAS
    query_student = db.session.query(Student).all()
    for student in query_student:
        query_course = db.session.query(Course).all()
        random.shuffle(query_course)
        query_course = query_course[0:num]
        for course in query_course:
            student.courses.append(course)
            db.session.commit()
