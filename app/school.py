from app import create_app

if __name__ == '__main__':
    school = create_app()
    school.run()
