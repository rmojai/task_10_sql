from flasgger import Swagger
from flask import Flask

import school_config


def create_app(config=None):
    app = Flask(__name__)
    if config is None:
        app.config.from_object(school_config.Config)
    else:
        app.config.from_object(config)

    from app.models import db
    db.init_app(app)

    with app.app_context():
        from app.create_data import create
        app.cli.add_command(create)

    from api.api_resource import api_school
    app.register_blueprint(api_school)

    Swagger(app, template_file='../api/school.yml')
    return app
