from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

relation = db.Table(
    'relation_students-courses',
    db.Column('student_id', db.Integer, db.ForeignKey('student.id', ondelete="CASCADE")),
    db.Column('course_id', db.Integer, db.ForeignKey('course.id', ondelete="CASCADE")))


class Group(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    group_name = db.Column(db.String(20), nullable=False)


class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.Integer, db.ForeignKey('group.id', ondelete="CASCADE"))
    first_name = db.Column(db.String(20), nullable=False)
    last_name = db.Column(db.String(20), nullable=False)
    courses = db.relationship('Course', secondary=relation, back_populates='students', cascade="all, delete", )

    def __init__(self, first_name, last_name, group_id=None):
        self.group_id = group_id
        self.first_name = first_name
        self.last_name = last_name


class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_name = db.Column(db.String(20), nullable=False)
    description = db.Column(db.String(200))
    students = db.relationship('Student', secondary=relation, back_populates='courses', cascade="all, delete", )
